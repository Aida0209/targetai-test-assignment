import React, { Component } from "react";
import "./styles.css";
import "./App.css";

interface Comment {
    id: number;
    word: string;
    comment: string;
};

interface IState {
    isText: boolean;
    openWindow: boolean;
    comments: Comment[];
    addingComment: string;
    selectedWord: string;
    text: string;
};

class App extends Component {

    state: IState = {
        isText: false,
        openWindow: false,
        comments: [],
        addingComment: '',
        selectedWord: '',
        text: `Beyond a simple condemnation of authoritarian value systems, Fromm used the story of Adam and Eve as an allegorical explanation for human biological evolution and existential angst, asserting that when Adam and Eve ate from the Tree of Knowledge, they became aware of themselves as being separate from nature while still being part of it. 
        This is why they felt "naked" and "ashamed": they had evolved into human beings, conscious of themselves, their own mortality, and their powerlessness before the forces of nature and society, and no longer united with the universe as they were in their instinctive, pre-human existence as animals. 
        According to Fromm, the awareness of a disunited human existence is a source of guilt and shame, and the solution to this existential dichotomy is found in the development of one's uniquely human powers of love and reason. 
        However, Fromm distinguished his concept of love from unreflective popular notions as well as Freudian paradoxical love (see the criticism by Marcuse below).`
    };

    getWord = () => {
        const selection = (window as any).getSelection();
        const selString = selection.toString();
        if (!!selString) {
            this.setState({
                selectedWord: selString,
                isText: true 
            });
        }
    };

    addComment = () => {
        const {selectedWord} = this.state;
        const regexp = /,+|\.+|-+|\s+/;
        const match = selectedWord.match(regexp);
        if (!!match) {
            alert('Возможно добавить комментарий только к одному целостному слову без пробелов, точек и запятых!')
        } else {
            this.setState({
                openWindow: true
            })
        }
    };

    applyComment = () => {
        const { addingComment, selectedWord, comments, text} = this.state;
        const id = comments.length + 1;
        const newComment = {id: id, word: selectedWord, comment: addingComment};
        
        const startPosition = text.indexOf(selectedWord);
        const newText = text.substring(0, startPosition + selectedWord.length) + '</span>' + text.substring(startPosition + selectedWord.length);
        const newTextFull = newText.substring(0, startPosition) + '<span class="App-span-comment">' + newText.substring(startPosition);
        this.setState({
            text: newTextFull,
            comments: [...comments, newComment],
            addingComment: ''
        });
    };

    onChangeComment = (e: any) => {
        this.setState({
            addingComment: e.target.value
        })
    };

    getText = () => {
        const { text } = this.state;
        return {__html: text };
    };

    render() {
        const { isText, openWindow, selectedWord, addingComment, comments, text } = this.state;

        return (
            <div className="App-main">
                {
                    openWindow && 
                    <div className="App-div-comment">
                        <p className="App-p">Выбранное слово: {selectedWord}</p> 
                        <input className="App-input" placeholder="Введите комментарий" value={addingComment} onChange={(e) => this.onChangeComment(e)}></input>
                        <button className="App-btn-ok" onClick={this.applyComment}>Добавить</button>
                    </div>
                }
                <div className="App-div-text" onMouseUp={this.getWord} dangerouslySetInnerHTML={this.getText()} />
                <button className="App-btn" onClick={this.addComment} disabled={!isText}>Добавить комментарий</button>
                {
                    comments.length > 0 &&
                    <table className="App-table">
                        <tr><th>Слово</th><th>Комментарий</th></tr>
                        { 
                            comments.map(item => {
                                return (
                                    <tr key={item.id}>
                                        <td>{item.word}</td><td> {item.comment}</td>
                                    </tr>
                                )
                            })
                        }
                    </table>
                }
            </div>
        );
    }
}

export default App;
